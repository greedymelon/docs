# Tableau bookmarks

May be outdated, keeping for reference.

Configure default perms properly:
<https://www.thedataschool.co.uk/stephanie-kearns/tableau-server-default-project-permissions/>

Lots of info on deep technical topics here:
<https://enterprisetableau.com/>

Frequent auth timeout misconfig:
<https://kb.tableau.com/articles/issue/intermittent-error-unable-to-sign-in-with-saml-sso-on-tableau-server>

Tune connections, maybe not totally applicable to Tableau Online
<https://kb.tableau.com/articles/issue/live-database-connection-not-closing>

Advanced extracts generation
<https://medium.com/snake-charmer-python-and-analytics/building-and-publishing-tableau-hyper-extracts-with-python-6ad096e7f3f3>

Accessing logs for optimization (article is about redshift, but likely applicable to other dbs)
<https://tableaulove.com/road-rules-tableau-redshift-part-2-logging/>

Log viewer
<https://github.com/tableau/tableau-log-viewer>

Slow extracts
<https://tarsolutions.co.uk/blog/tableau-server-extract-slow-to-refresh/>

Viewing underlying sql queries (probably also possible on Online)
<https://kb.tableau.com/articles/howto/viewing-underlying-sql-queries-desktop>

Logshark
<https://github.com/tableau/Logshark>

Tableau's github repo, some tools there
<https://github.com/tableau>

Matrix of optimizations
<https://public.tableau.com/views/BestPracticesforDashboardPerformance/Factorsaffectingdashboardperformance?:showVizHome=no>

General Tableau Q&A
<https://www.edureka.co/blog/interview-questions/top-tableau-interview-questions-and-answers/>

Use Python in Tableau (not sure applicable to Online)
<https://tableau.github.io/TabPy/>

Understanding Tableau queries, techniques and tools to use
<https://www.youtube.com/watch?app=desktop&v=xIRjiczt1r8>

Tableau RLS
<https://www.tableau.com/learn/whitepapers/row-level-security-entitlements-tables>

Embedding
<https://help.tableau.com/current/api/embedding_api/en-us/index.html>

Changing datasource server name in batch
<https://tableaufriction.blogspot.com/2012/12/changing-database-server-name-in.html>

old reference: <https://tableauandbehold.com/2016/03/07/how-to-set-up-your-database-for-row-level-security-in-tableau/> (not sure if still applicable)

Data source design
<https://robertlambert.net/2020/03/two-design-principles-for-tableau-data-sources/>

Dashboard performance tips
<https://smartbridge.com/how-increase-tableau-dashboard-performance/>

Datasource publishing best practices
<https://help.tableau.com/current/pro/desktop/en-us/publish_datasources_about.htm>
