[toc]

# Interns' training course plan ideas

**This is the result of a conversation with ChatGPT4**

*Q: structure of a guide for Interns on system administration (I gave it tips
on what topics to cover, though). Some of the topics are covered in the rest of this document, others are not, it's all a WIP*

The guide can be divided into several sections, each dealing with different
aspects of system management and development. Here's a potential structure:

- Introduction to Linux
    - Basic command line operations: `ls`, `cd`, `mkdir`, `rm`, etc.
    - Filesystem hierarchy: understanding directories like `/etc`, `/var`, `/home`, etc.
    - Process management: `ps`, `top`, `htop`, `kill`, etc.
    - Package management using `yum` or `apt-get`
    - Editing files using `vim` or `nano`
    - Permissions and ownership

- Networking
    - Basic networking concepts: IP, TCP, UDP, DNS, DHCP
    - Network troubleshooting commands: `ping`, `netstat`, `ifconfig`, `traceroute`
    - Firewalls and security: `iptables`, `ufw`
    - Understanding `ssh`

- System Architecture
    - Understanding the Linux Kernel
    - User space vs Kernel space
    - Understanding system calls
    - Memory management in Linux

- Monitoring
    - Using monitoring tools: `top`, `htop`, `netstat`, `iostat`
    - Log management: `syslog`, `journalctl`
    - Performance monitoring and stress testing

- Python
    - Basic Python syntax and structures
    - Writing scripts to automate tasks
    - Using Python for web development with Django or Flask
    - Python for data science with Pandas and NumPy

- Security
    - Basic security principles
    - Understanding and applying user permissions
    - Securing network services
    - Understanding SSL/TLS and HTTPS
    - Using encryption tools

- Cloud Infrastructure
    - Understanding cloud computing concepts
    - Basics of AWS, Google Cloud, and Azure
    - Deploying applications in the cloud
    - Understanding and using Docker and Kubernetes
    - Infrastructure as Code (IaC) using Terraform or CloudFormation

---

# Secure Shell (SSH)

## Overview of SSH Concepts

SSH, or Secure Shell, is a protocol used to securely log onto remote systems.
It provides a secure channel over an unsecured network in a client-server
architecture. It uses public key encryption: the **.pub** key can be sent to
anyone, or put in any machine’s `/.ssh/authorized_keys` to allow access. What
must always be kept private is your private key, and it's only ever needed on
the machine when you initiate the connection from. The private / public key
pair is by default `~/.ssh/id_rsa` and `~/.ssh/id_rsa.pub`. To create the pair
initially, just run `ssh-keygen`

## SSH Uses

- Remotely accessing servers or hosting services
- Transferring files using SCP or SFTP
- Tunneling & port forwarding
- Managing Git repositories

## Caveats

- Although SSH itself is secure, its security is only as strong as its weakest
  link. If a user's private key is compromised, an attacker can access all
  systems that accept that key. This is why the permissions on the private key
  are set to `0600`
- SSH is susceptible to brute force attacks if password authentication is used;
  mitigation is possible, but the simplest is to disable password
  authentication and just keep private key authentication available on any
  server by editing `~/.ssh/sshd_config`

## Sample SSH Config file (for MacOS)

Here's a (short) commented `ssh_config` file:

```bash
# Host-specific Settings
Host example
    HostName example.com     # Real host name
    User john                # Username
    Port 2222                # Port number
    IdentityFile ~/.ssh/id_rsa_example  # Path to private key for this host

# Global Settings
Host *                        # Applies to all hosts
    ForwardAgent yes          # Enables forwarding of the authentication agent connection
    AddKeysToAgent yes        # load the keys in the agent when they're used
    UseKeychain yes           # store the passphrase in the MacOS keychain
    ServerAliveInterval 300   # Checks if server is alive every 300 seconds
```

## Tips

- Always use key-based authentication instead of passwords, regularly update
  your SSH keys, and use strong passphrases for your keys.
- Use `ssh-agent` for managing your keys (see below)
- You can use `ssh -v` for verbose mode, which is useful for debugging.
- To manually add an ssh key to the ssh-agent, use `ssh-add ~/.ssh/<private key
  file>`.
- To manually add the ssh key to the Mac OS keychain, use `ssh-add -K
  ~/.ssh/id_rsa`. This will store the passphrase in the keychain, and you won't
  have to enter it every time you use the key.

# Additional SSH Topics

## SSH Tunnels

SSH tunnels can be used to forward traffic from client to server or from server
to client for specific ports. This can be very useful for accessing remote
resources securely.

### Local Port Forwarding

Local port forwarding allows you to forward traffic from your local machine to
the remote server. This is useful for accessing a remote service as if it were
local.

Here's an example:

```bash
ssh -L 9000:localhost:3306 user@yourserver.com
```

In this example, connections to `localhost:9000` on your local machine will be
forwarded to `localhost:3306` on `yourserver.com`. Note it doesn't have to be
localhost, the forwarding can go to any machine; this is the method used on SSH
bastion hosts, like (to access a MySQL instance as if it were running locally
on port 33306):

```bash
ssh -L 33306:mysql.inside.example.com:3306 myusername@bastion.outside.example.com
```

### Remote Port Forwarding

Remote port forwarding allows you to forward traffic from the remote server to
your local machine. This is useful for allowing others to access a service on
your local machine.

Here's an example:

```bash
ssh -R 9000:localhost:3306 user@yourserver.com
```

In this example, connections to `localhost:9000` on `yourserver.com` will be
forwarded to `localhost:3306` on your local machine. This is useful only in
specific scenarios though, but just know that the possibility exists.

## SSH-Agent and Key Forwarding

SSH-Agent is a program that runs in the background and stores your private
keys. When SSH needs to authenticate against a private key, it can use the keys
stored in SSH-Agent. This allows you to use key-based authentication without
needing to enter your passphrase every time.

On a Mac, it is launched automatically when using **ssh**, thanks to the
**launchd** service in
`/System/Library/LaunchAgents/com.openssh.ssh-agent.plist` that listens on a
Unix socket, which path is contained in the `SSH_AUTH_SOCK` environment
variable.

If not on a Mac, to start the ssh-agent in the background:

```bash
eval "$(ssh-agent -s)"
```

On all systems, to add your SSH private key to the ssh-agent:

```bash
ssh-add ~/.ssh/id_rsa
```

Alternatively, specifying `IdentityFile <path to a private key>` and
`AddKeysToAgent yes` in a Host section or for the catch-all `Host *` section in
`~/.ssh/config` will do the same and load the key in the agent automatically
(see the sample ssh config above).

Key forwarding allows you to use your local SSH keys instead of leaving keys
(potentially unsecured) on the server. Reminder: your private key should
probably never leave your machine, and this is what agent forwarding allows. To
enable agent forwarding, you can add the following to your `~/.ssh/config`
file:

```bash
Host *
  ForwardAgent yes
```

Then, when you SSH into a server, you can use your local keys to authenticate
on any other servers:

- from the machine that has your private key: `ssh user@server1`
- once logged in server1, you can run `ssh user@server2` without your private
  key being installed on server1, and so on.

*Note: Be aware that key forwarding should be used judiciously. If the remote
server is compromised, the attacker could use the forwarded agent connection to
authenticate using your keys.*

---

# Introduction to Linux

**David's recommendation: before anything, install the `cheat` and/or `tldr`
command line tools for quick reference.**

- [https://opensource.com/article/22/6/linux-cheat-command][1]
- [https://tldr.sh/][2]

Also, make sure you have **zsh completions** installed and configured, it will
make your life a lot easier, by displaying the available commands options as you
type, by hitting the TAB key, example:

![alt text](./interns-training-withAI.assets/CleanShot%202024-03-08%20at%2011.07.11.png)

For this, an easy way can be to install *oh-my-zsh* and *powerlevel10k*, which
require minimal configuration and provide a lot of features.

For most of the commands below, `<command> --help` will give you some basic
hints, but make sure to read the tips on the 2 urls/tools mentioned above.
Here's an example with `tldr cp`:

![alt text](<interns-training-withAI.assets/CleanShot 2024-03-14 at 09.25.26@2x.png>)

## Basic Command Line Operations

- `ls`: This command lists all files and directories in the current directory.
- `cd`: Change the current directory. Usage: `cd [directory name]`.
- `mkdir`: Creates a new directory. Usage: `mkdir [directory name]`.
- `rm`: Deletes a file. Usage: `rm [file name]`. Use `-r` option for deleting
  directories. Use `-f` (=no confirmation) carefully. More than one user has
  `run rm -rf *` and deeply regretted it.
- `cp`: Copies files or directories. Usage: `cp [source] [destination]`.
- `mv`: Moves or renames files or directories. Usage: `mv [source]
  [destination]`.
- `ln`: create hardlinks or symlinks

## Process Management

- `ps`: Displays the processes that are currently running in the system. Takes
  many options, usual ones would be `ps aux` or `ps afxe` for instance.
- `top`: Displays the real-time list of all running processes.
- `htop`: An interactive process viewer, similar to `top` but more
  user-friendly. Hint: set it to hide userland threads or the output will be
  very long, configurable with F10.
- `kill`: Terminates a process. Usage: `kill [signal] [PID]`. To kill a process
  group, use `kill [signal] -[PID of the parent (PPID in "top")]`
- `pgrep`, `pkill`: process management using regex matches

## Permissions and Ownership

Every file and directory in Linux has an associated owner and group, and three
types of permission access: read (r), write (w), and execute (x). Most
filesystems also support extended attributes, but they're a bit out of our
scope here. Same goes for SUID, SGID and sticky bits. You can look them up.

- `chmod`: Changes the permissions of a file or directory.
  Usage: `chmod [permissions] [file/directory]`.
- `chown`: Changes the ownership of a file or directory.
  Usage: `chown [user]:[group] [file/directory]`.
- `chgrp`: Changes the group ownership of a file or directory.
  Usage: `chgrp [group] [file/directory]`.

All these commands are flexible, for instance all of them support the -R
(recursive) flag.

## Package Management

Linux distributions use package managers to install, update or remove software.

- `yum` or `dnf`: The package managers for CentOS, Fedora, and Amazon Linux.
- `apt-get` or `apt`: The default package manager for Ubuntu and other
  Debian-based distributions.

## Editing Files

- `vim`: A highly configurable text editor built to enable efficient text editing.
- `nano`: An easy-to-use text editor with a simple interface.

### Super basic Vim guide

#### Opening Vim and Creating Files

- To open Vim, simply open your terminal and type `vim`.
- To create or open a file with Vim, type `vim filename` where filename is the name of the file you want to create or open.

#### Modes

Vim operates in various modes, the most basic ones are Command mode, Insert mode, and Visual mode

- Command mode: this is the default mode when you open Vim; you can execute
   commands in this mode. To return to it from another mode, usually type `Esc`.
- Insert mode: This mode allows you to insert or edit text; to enter Insert mode from Command mode, press `i`.
- Visual mode: Allows to copy text (and more); to enter Visual mode from Command mode, press `v`.

#### Basic Navigation

- `h`: move left
- `j`: move down
- `k`: move up
- `l`: move right

These keys are used for navigation in Command mode. You can also use the arrow keys, which are also supported in visual mode, but it's recommended to use the hjkl keys to keep your hands on the home row.

#### Basic Editing

- To start inserting text, press `i` to switch to Insert mode.
- To stop inserting text and return to Command mode, press `Esc`.
- To copy text, type `v` from Command mode, highlight text by moving the cursor, then type `y`. It will return to command mode, then you can move the cursor and type `p` to insert the copied text.
- To delete a line in Command mode, type `dd`; to paste it somewhere, just type `p`. To delete 2 lines, type `2dd`, etc.
- More to explore: various visual mode possibilities, like `shift-v` (multiline select) and `ctrl-v` (block select)
- Search: in Command mode type `/` followed by a (regex) pattern

#### Saving and Quitting

- To save a file, type `:w` in Command mode and press Enter.
- To quit Vim, type `:q` in Command mode and press Enter.
- To save and quit at the same time, type :wq.

#### Undo and Redo

- To undo, type `u` in Command mode.
- To redo, type `Ctrl-r` in Command mode.

---

# System Architecture

*May be -partly- redundant with the above, but teaching is repeating, right?*

## Init and Systemd

**Init** is the first process started during system boot. It is a daemon
process that continues running until the system is shut down.

**Systemd** is a newer init system that has become the standard for many Linux
distributions. It is responsible for bootstrapping the user space and managing
all subsequent processes. It starts services in parallel, which can lead to

faster boot times, and manages dependencies between various units.
Systemd uses units to manage resources. Services are defined by service units,
which are configuration files ending in `.service`. For example, to start a
service, you'd use `systemctl start [service]`, and to enable it on boot, you'd
use `systemctl enable [service]`.

## Filesystem Hierarchy

- `/`: The root directory where the whole tree starts.
- `/etc`: Contains configuration files required by all programs.
- `/var`: This directory contains variable data like system logging files, mail
  and printer spool directories, and transient and temporary files.
- `/home`: Home directories for all users to store their personal files.
- `/bin`: Contains base binary executables.
- `/lib`: Contains base libraries (alias to /usr/lib on recent distros) and
  kernel modules
- `/usr/bin`, `/usr/share`, `/usr/lib`: readonly extra binaries and libraries,
  usually the largest directories in a Linux install
- `/proc` and `/sys`: system volatile directories containing configuration and
  accounting dynamic information

## Mount Points

Typically a GNU/Linux system will have the largest disk partition formatted in
Ext4, and mounted on `/` (the slash is the filesystem separator sign on Unix).

Mount points are the locations in the directory hierarchy where other
filesystems (other partitions, disks, network volumes, etc) are mounted. For
example, `/mnt` is a common directory to mount temporary filesystems, and
`/media` is where removable media like CDs, DVDs, USB drives are mounted.

## Kernel and Modules

The Linux **kernel** is the core of the operating system. It interacts with the
system's hardware and handles tasks like process management, memory management,
and device drivers.

**Kernel modules** are pieces of code that can be loaded and unloaded into the
kernel upon demand. They extend the functionality of the kernel without the
need to reboot the system. For example, one type of module is the device
driver, which allows the kernel to access hardware connected to the system.

## Processes

A **process** is an instance of a running program. When a program is executed,
the system creates a special environment that contains everything needed for
the program to run. This environment is a process.

Processes are managed by the kernel. They can spawn child processes but all
processes are started by the init process. Linux provides commands like `ps`,
`top`, `htop` for process monitoring, and `kill`, `pkill`, `killall` for
process termination.

## Users and Permissions

Every file and directory in Linux is assigned three types of owner, namely:

- User: A single user who is the owner of file or directory
- Group: A group of users who share permissions for a file or directory
- Other: Any other users who have access to the file

Each file or directory has defined permissions that specify the actions that
can be performed by the user, the group, and others. These permissions include:

- Read: Users can read the contents of the file or list the contents of the
  directory.
- Write: Users can write or modify the file or directory.
- Execute: Users can run the file or read, write and execute the contents of
  the directory.

These permissions can be changed by the `chmod`, `chown` and `chgrp` commands.
They are also represented numerically: 4 (read), 2 (write), 1 (execute).

For example, to change the permissions of a file to read, write, and execute
for the user, read and execute for the group, and only read for others, you
    would use `chmod 754 filename`.

---

# Sudo

`sudo` stands for "SuperUser DO". It is a powerful command used in Unix and
Linux operating systems that allows a user to run programs with the security
privileges of another user (by default, the superuser or root).

Running commands as a superuser or root user gives you unrestricted access to
perform tasks that are typically restricted, such as installing software,
changing file ownership, modifying system files, etc.

Here are some examples of using `sudo`:

1. **Installing a software**: Regular users do not have permissions to install
software on a system-wide basis. However, with `sudo`, you can install software
using package managers like `apt` or `yum`. Example: `sudo apt install htop`

2. **Managing services**: Only the superuser or root can start, stop, or restart
services on a Linux system. If you are managing a web server and need to restart
the Apache service, you would use `sudo`: `sudo systemctl restart apache2`

3. **Modifying system files**: Some configuration files in the `/etc/` directory
require root access to modify. For example, to modify the `hosts` file, you
might use `sudo` with a text editor: `sudo vim /etc/hosts`

Use `visudo` to edit the sudoers file: the `sudoers` file contains the rules
that govern `sudo` access, in particular which users and/or groups can use it.
Use the `visudo` command to edit this file, as it performs syntax checking to
prevent errors that could lock you out of `sudo` access.

Typically, members of an`adm`, `admin` or `sudo` group would be granted this
ability (depending on the distribution), and no editing of the sudoers file is
ever needed, access is provided if the user is granted membership of the
relevant group.

Tip: to impersonate root for some time (so you don't have to type sudo all the time)
you can use `sudo su -`. Make sure you understand what it does :-)

---

# Shell redirections

powerful feature in Linux that allows you to control where the input to a
command comes from, or where the output goes. Here are some examples:

1. **Standard Output (STDOUT) Redirection**: The `>` operator is used to
redirect the output of a command to a file, overwriting the file's existing
contents. For example, to redirect the output of the `ls` command to a file
named `filelist.txt`, you would do:

```bash
ls > filelist.txt
```

If you want to append the output to the file instead of overwriting it, use the `>>` operator:

```bash
ls >> filelist.txt
```

2. **Standard Error (STDERR) Redirection**: The `2>` operator is used to
redirect the error output of a command to a file. For example, to redirect the
error output of the `ls` command to a file named `error.txt`, you would do:

```bash
ls /nonexistentdirectory 2> error.txt
```

Similarly, to append the error output to the file, use `2>>`:

```bash
ls /nonexistentdirectory 2>> error.txt
```

3. **Redirecting Both STDOUT and STDERR**: You can redirect both the standard
output and standard error to the same file with `&>`:

```bash
ls /nonexistentdirectory &> output_and_error.txt
```

4. **Standard Input (STDIN) Redirection**: The `<` operator is used to read
input from a file. For example, you can use it to provide input to the `sort`
command:

```bash
sort < filelist.txt
```

5. **Pipelines (`|`)**: The pipe operator is used to use the output of one
command as input to another. For example, to list files in a directory and pipe
the output into `grep` to search for a specific file:

```bash
ls | grep myfile.txt
```

6. **Redirecting within a script**: You can also redirect all output of a script
to a file:

```bash
#!/bin/bash
exec > logfile
echo "This is a test"
```

In this case, the `exec` command is used to redirect all subsequent commands in
the script to the specified file.

Remember, always be careful when using redirection, especially when using `>` or
`2>`, as it can overwrite existing files.

---

# Subshells

Subshells are child processes launched by a shell or shell script, which can
run commands independently of the parent shell. The output from a subshell can
be captured and used in the parent shell. Here are some examples:

1. **Executing multiple commands**: You can use a subshell to execute multiple
   commands and use their combined output in the parent shell:

```bash
echo "Today's date is: $(date +%D), and the current time is: $(date +%T)"
```

In this example, two subshells are created to execute the `date` command with
different options. Their outputs are interpolated in the `echo` command.

2. **Changing directory**: If you change the directory in a subshell, it
   doesn't affect the parent shell:

```bash
echo "Current directory: $(pwd)"
(cd /tmp; echo "After changing directory: $(pwd)")
echo "Back in the original directory: $(pwd)"
```

In this example, a subshell is used to change the directory to `/tmp` and print
the new location, but after exiting the subshell, you are back in the original
directory.

3. **Running a background job**: You can use a subshell to run a long command
   in the background while continuing to use the terminal:

```bash
(sleep 30; echo "Done!") &
```

In this example, the `sleep` command runs in a subshell in the background, and
the parent shell is immediately available for other tasks.

4. **Capturing the output of a sequence of commands**: Subshells allow you to
   capture the output of a sequence of commands as if it were a single command:

```bash
myfiles=$(cd /etc; ls *.conf)
```

In this example, the subshell changes to the `/etc` directory and lists `.conf`
files. The output is then stored in the `myfiles` variable.

Subshells provide a convenient way to group commands and use their output in a
shell or script, and they can make your scripts more readable and maintainable.

---

# useful commands for sysadmins

Sure, here are some practical examples for each of the commands:

1. **ps**
    - `ps aux`: This will list all the current running processes along with
      detailed information such as user ID, CPU usage, memory usage, command
      name, etc.
    - `ps -ef | grep httpd`: This will list all processes and then filter for
      'httpd', useful for checking if a specific service (in this case, Apache)
      is running.
    - `ps -eo pid,ppid,cmd,%mem,%cpu --sort=-%mem | head`: This will display
      the top processes sorted by memory usage.

2. **top / htop**
    - `top -u username`: This will display only the processes of a specific
      user.
    - `top -n 1 -b > top-output.txt`: This will run `top` once and save the
      output to a file.
    - `htop -d 10`: This will refresh `htop` every 10 seconds.

3. **netstat**
    - `netstat -tuln`: This will display only listening sockets along with the
      protocol type and the services' names.
    - `netstat -r`: This will display the routing table.
    - `netstat -i`: This will display a table of all network interfaces.

4. **lsof**
    - `lsof -u username`: This will list all files opened by a specific user.
    - `lsof /path/to/file`: This will find out which process has a particular
      file open.
    - `lsof -i :22`: This will show which process is using network port 22
      (SSH).

5. **df**
    - `df -h`: This will display disk usage in human-readable format.
    - `df -T`: This will display the file system type along with disk usage.
    - `df -h /home`: This will display disk usage of '/home' directory.

6. **du**
    - `du -sh /home/*`: This will display the size of each directory in '/home'
      in human-readable format.
    - `du -ah | sort -rh | head -n 20`: This will display the 20 largest files
      or directories.
    - `du -sh --exclude="*.mp4" /folder`: This will calculate the size of
      '/folder' excluding .mp4 files.

7. **free**
    - `free -m`: This will display the amount of free and used memory in
      megabytes.
    - `free -h`: This will display the memory details in human-readable format.
    - `free -s 10`: This will update the free command every 10 seconds.

8. **ifconfig / ip**
    - `ifconfig eth0`: This will display the configuration for 'eth0' network
      interface.
    - `ip addr show`: This is the modern equivalent of 'ifconfig' to display all
      network interfaces.
    - `ip addr add 192.168.1.10/24 dev eth0`: This command adds the IP address
      192.168.1.10 to the network interface 'eth0'.
    - `ifconfig eth0 down`: This command disables the 'eth0' network interface.

9. **ping**
    - `ping -c 4 www.google.com`: This command sends 4 ICMP Echo Requests to
      \<www.google.com\> to check the network connectivity.
    - `ping -i 10 192.168.1.1`: This command sends an ICMP Echo Request every
      10 seconds to the IP address 192.168.1.1.
    - `ping -w 10 192.168.1.1`: This command sends ICMP Echo Requests to the IP
      192.168.1.1 for 10 seconds.

10. **ssh**
    - `ssh user@server`: This command initiates a SSH connection to 'server' as
      'user'.
    - `ssh -p 2222 user@server`: This command initiates a SSH connection to
      'server' on port 2222 as 'user'.
    - `ssh -i /path/to/key.pem user@server`: This command initiates a SSH
      connection to 'server' using a specific key file.

11. **scp / rsync**
    - `scp file.txt user@server:/path`: This command copies 'file.txt' to 'server'
    under '/path' as 'user'.
    - `rsync -avz /path/to/source/ user@server:/path/to/destination/`: This
    command syncs files from local to remote server using rsync over SSH.
    - `rsync -avz --delete /source/directory /destination/directory`: This command
    syncs directories and deletes files in the destination that are not present
    in the source.

12. **iptables**
    - `iptables -L`: This command lists all the rules in the default table
      (filter).
    - `iptables -A INPUT -p tcp --dport 22 -j ACCEPT`: This command adds a rule
      to allow incoming traffic on port 22 (SSH).
    - `iptables -P INPUT DROP`: This command sets the default policy for
      incoming traffic to DROP.

13. **crontab**
    - `crontab -l`: This command lists all the cron jobs for the current user.
    - `crontab -e`: This command opens the current user's crontab file in the
      default text editor for modification.
    - `*/10 * * * * /path/to/script.sh`: This is an example of a cron job that
      runs '/path/to/script.sh' every 10 minutes.

14. **useradd / usermod / userdel**
    - `useradd -m username`: This command creates a new user 'username' and a
      home directory for them.
    - `usermod -aG sudo username`: This command adds 'username' to the 'sudo'
      group, giving them superuser privileges.
    - `userdel -r username`: This command removes 'username' and their

15. **chmod / chown / chgrp**
    - `chmod 755 /path/to/file`: This command changes the permissions of
      '/path/to/file' to 755 (read and execute permissions for everyone and
      additionally write permission for the owner of the file).
    - `chown username:group /path/to/file`: This command changes the ownership
      of '/path/to/file' to 'username' and the group to 'group'.
    - `chgrp group /path/to/file`: This command changes the group ownership of
      '/path/to/file' to 'group'.

16. **journalctl**
    - `journalctl -b`: This command shows all messages from the current boot.
    - `journalctl -f`: This command allows you to watch the journal in
      real-time, similar to tail -f.
    - `journalctl -u sshd`: This command shows all messages for the sshd unit,
      which is useful for troubleshooting SSH issues.

17. **systemctl**
    - `systemctl status sshd`: This command shows the status of the sshd
      service.
    - `systemctl stop httpd`: This command stops the httpd service.
    - `systemctl restart nginx`: This command restarts the nginx service.

18. **sudo**
    - `sudo apt-get update`: This command updates the package lists for
      upgrades for packages that need upgrading, as well as new packages that
      have just come to the repositories.
    - `sudo useradd john`: This command creates a new user named 'john' with
      superuser privileges.
    - `sudo reboot`: This command reboots the system with superuser privileges.

---

# Logging and Log Rotation

## Location of Important Log Files

1. **/var/log/syslog**: This is the file where you'll find most of the logging
   messages from the system and various applications. It includes information
   such as system startup messages, user logins, and system services.

2. **/var/log/auth.log**: This file contains system authorization information,
   including user logins and authentication machinsm that were used.

3. **/var/log/kern.log**: This log file is dedicated to kernel messages. It's
   useful when debugging hardware and connectivity issues.

4. **/var/log/dpkg.log**: This log file is where all binary package log
   information is stored. You can refer to dpkg.log if you need to check the
   installation or removal of packages.

5. **/var/log/apt/history.log**: This log file records the commands that are
   run by the apt command-line utility.

6. **/var/log/faillog**: This file contains user login failures. It's useful to
   check if the system is under attack or if a specific user is having trouble.

7. **/var/log/boot.log**: This file contains system boot information, which can
   be helpful for troubleshooting system startup issues.

## Log Rotation

In Debian, the log rotation is handled by the logrotate utility. The
configuration files for logrotate are located in `/etc/logrotate.conf` and
`/etc/logrotate.d/`. Each logfile can have its configuration file, where you
can specify parameters like the frequency of rotation, compression, number of
copies to keep, and more.

For example, the default rotation for the syslog file might look like this:

```text
/var/log/syslog
{
    rotate 7
    daily
    missingok
    notifempty
    delaycompress
    compress
    postrotate
        invoke-rc.d rsyslog rotate > /dev/null
    endscript
}
```

This configuration rotates the syslog daily, keeps 7 copies, compresses the log
files, and restarts the rsyslog service after rotation.

## Routing of Events

The routing of events to various log files is handled by the syslog daemon. In
Debian, the default syslog daemon is rsyslog. The configuration file for
rsyslog is `/etc/rsyslog.conf`, and additional custom files can be added in the
`/etc/rsyslog.d/` directory.

In the configuration file, you'll find lines that map facilities and priorities
to log files. For example:

```text
auth,authpriv.*                 /var/log/auth.log
*.*;auth,authpriv.none          -/var/log/syslog
```

The first line directs any message from the `auth` and `authpriv` facilities to
`/var/log/auth.log`. The second line sends all messages of any facility and
priority to `/var/log/syslog`, except for `auth` and `authpriv`.

## Tailing and Searching Logs

The `tail` command is used to display the end of a file. `tail -f
/var/log/syslog` will display the most recent entries and update in real-time.

To search in log files, you can use `grep`. For example, `grep "error"
/var/log/syslog` will find all lines that contain the word "error".

You can also combine these two commands. For example, `tail -f /var/log/syslog
| grep ssh` will display real-time log entries that contain the word "ssh".

---

# Pyenv

Pyenv is a powerful tool for managing multiple Python versions. It allows you
to switch between different Python versions on a per-project basis, ensuring
that each of your projects can have its own Python environment, independent of
the others.

## How Pyenv Works

Pyenv works by intercepting Python commands using shim executables injected
into your `PATH`. When you execute a Python command, `pyenv` determines which
Python version has been specified for your application (by `pyenv local`,
`pyenv global`, or `pyenv shell`) and passes the command along to the
appropriate Python installation.

## How Pyenv Manages the PATH

When you install `pyenv`, it asks you to add `~/.pyenv/shims` and
`~/.pyenv/bin` to your `PATH`. The `~/.pyenv/shims` directory contains shims
for every Python command across every installed version of Python — `python`,
    `pip`, `idle`, `pydoc`, etc. These shims are lightweight executables that
    simply pass your command along to `pyenv`.

When you run a Python command, the operating system checks directories in your
`PATH` from left to right until it finds a directory where the command exists.
Pyenv shims take advantage of this by intercepting all Python commands,
regardless of which Python version was intended to be used.

## How Pyenv Manages Installation of Dependencies

Each Python version installed through `pyenv` is completely isolated from the
system Python and other Python versions managed by `pyenv`. This means that
when you install packages using `pip` while in a `pyenv` environment, those
packages will not be available when you switch to another Python version or to
the system Python. This allows for each project to have its own dependencies,
without conflicts.

## Manipulated PYTHON Environment Variables

`pyenv` manipulates the `PYTHONPATH` and `PYTHONHOME` environment variables.

- **PYTHONPATH**: It's a list of paths that Python checks when it's loading a
  module. `pyenv` uses this variable to ensure that Python code is executed
  with the correct Python version.

- **PYTHONHOME**: It's used to set the location of the standard Python
  libraries. `pyenv` sets this variable to the `lib/` directory of the selected
  Python version.

By manipulating these variables, `pyenv` ensures that the correct Python
interpreter and libraries are used when you execute Python code.

---

# Poetry

Poetry is a tool for Python application package management. It allows you to
declare the libraries your project depends on and Poetry will manage
(install/update) them for you.

## How Poetry Works

Poetry uses `pyproject.toml` to manage project dependencies, configurations,
and other related things. Once you specify your dependencies in the
`pyproject.toml` file, Poetry can install them in an isolated environment.

## How Poetry Manages Dependencies
When you specify a dependency in Poetry, you're writing it to `pyproject.toml`,
and the corresponding lock file is `poetry.lock`. This file locks the installed
dependencies to a specific version, and it is this file that is used to install
dependencies when you share your project with others.

When you run `poetry install`, Poetry looks for the `poetry.lock` file. If it
exists, it installs the exact versions from there. If not, it creates one after
the installation. This ensures that everyone working on the project shares the
exact same dependencies.

## How Poetry Handles Environments

Poetry creates a virtual environment for each project to isolate its
dependencies from other projects. It's similar to other Python packaging tools
like pipenv, but it's more strict about ensuring the use of the virtual
environment.

When you run `poetry shell`, you're placed in the virtual environment for the
current project. You can also use `poetry run` to run individual commands
within the environment, for instance: `poetry run python3.10 yourscript.py`

## Poetry and PYTHON Environment Variables

Poetry does not directly manipulate Python environment variables like
`PYTHONPATH` or `PYTHONHOME`. Instead, it installs packages in an isolated
environment, ensuring that Python will find them without needing to modify
`PYTHONPATH`.

However, you can use the `poetry env use` command to specify a particular
Python version for your environment. Poetry will then use that version of
Python, without you needing to modify `PYTHONHOME` or other environment
variables.

---

# AWS IAM introduction

AWS Identity and Access Management (IAM) is a web service that helps you
securely control access to AWS resources for your users. You use IAM to control
who can use your AWS resources (authentication) and what resources users can
use in what ways (authorization).

Here are some key concepts within IAM:

## 1. IAM User

An IAM user is an entity that you create in AWS. The IAM user represents the
person or service who uses the IAM user to interact with AWS. A primary use for
IAM users is to give people the ability to sign in to the AWS Management
Console for interactive tasks and to make programmatic requests to AWS services
using the API or CLI.

## 2. IAM Group

An IAM group is a collection of IAM users. You can and should use groups to specify
permissions for multiple users, which can make it easier to manage the
permissions for those users.

## 3. IAM Role

An IAM role is an IAM identity that you can create in your account that has
specific permissions. It is similar to a user as it is an AWS identity with
permission policies that determine what the identity can and cannot do in AWS.
However, instead of being uniquely associated with one person, a role is
intended to be assumable by anyone who needs it.

**This is the preferred way of granting permissions, rather than granting them
directly to users (please, don't do it) or to groups. The only permission a
user or a group really needs is `sts:assumeRole` on one or several roles, and
all policies granting permissions should be attached to the various assumable
roles.**

## 4. IAM Policy

An IAM policy is an object in AWS that, when associated with an identity or
resource, defines their permissions. AWS evaluates these policies when an IAM
**principal** (user or role) makes a request. Permissions in the policies
determine whether the request is allowed or denied.

## 6. IAM Root User

When you first create an AWS account, you begin with a single sign-in identity
that has complete access to all AWS services and resources in the account. This
identity is called the AWS account root user and is accessed by signing in with
the email address and password that you used to create the account.

*It's recommended to not use this account and to instead create IAM users and
roles for everyday tasks.*

## 7. Permissions Boundary

A **permissions boundary** is an advanced feature in which you set the maximum
permissions that an identity-based policy can grant to an IAM entity (IAM user
or role). An entity's permissions boundary allows it to perform only the
actions that are allowed by both its identity-based policies and its
permissions boundaries. Let's say it is a **safety net for policies**.

## 8. Identity Federation

Identity federation is the act of establishing trust between an external
identity provider and AWS. Once trust is established, you can grant federated
users permissions to access your AWS resources. You can federate your users
into AWS using SAML 2.0, OpenID Connect (OIDC), etc.

This is what allows connecting to AWS via Google Workspace for instance; the federated users are managed via `IAM Identity Center`, a distinct service (or rather, an extension) from pure `IAM`.

---

# AWS IAM assuming a role

In order for an AWS IAM user to assume a role, several components need to work together:

1. **IAM User**: The IAM user is the identity that is going to assume the role.
   The user must have permissions to assume the role. This is usually defined
   in the IAM user's policy. Here's an example of a user policy in JSON that
   allows the user to assume a role:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "sts:AssumeRole",
      "Resource": "arn:aws:iam::123456789012:role/ExampleRole"
    }
  ]
}
```

2. **IAM Role**: The IAM role is the set of permissions that the user wants to
   assume. It has a trust policy that defines who can assume the role. Here's
   an example of a role trust policy in JSON that allows the user to assume the
   role (so to sum it up, a user must be explicitly allowed to assume role, but
   role must also explicitly allow the user to assume it):

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Principal": {"AWS": "arn:aws:iam::123456789012:user/ExampleUser"},
      "Action": "sts:AssumeRole",
      "Condition": {}
    }
  ]
}
```

3. **AWS Security Token Service (STS)**: AWS STS is the service that provides
   temporary security credentials when you assume a role. When the user assumes
   the role, AWS STS returns the set of temporary security credentials that the
   user can use to make requests to AWS.
   For instance, when running `aws sts assume-role <role arn>`, the API will
   return temporary credentials (`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`,
   `AWS_SESSION_TOKEN`) that should be used in later calls to the API. Using
   these will result in the user assuming the identity of the role.<br /><br />
   *It's just like renting a cocktail suit to be allowed at a James Bond movie
   premiere. If you don't put on the suit, you won't get in. If you do, you can
   raid the buffet.*

4. **Permissions Policy**: The permissions policy is attached to the role and
   defines what actions are allowed or denied by the role. Here's an example of
   a permissions policy in JSON:

```json
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": "s3:ListBucket",
      "Resource": "*"
    }
  ]
}
```

To sum it up, in order for the user to assume the role:

- The user must have permission to assume the role, as defined in the user policy.
- The role's trust policy must allow the user to assume the role.
- The role must have a permissions policy that grants the desired permissions.
- The user calls STS and requests temporary credentials, and is granted access
  to the resources

---

# AWS assume role examples

Here's how you can assume a role using both the AWS CLI and Python:

## 1. AWS CLI

To assume a role via the AWS CLI, you can use the `aws sts assume-role` command. Here's an example:

```bash
aws sts assume-role \
    --role-arn "arn:aws:iam::123456789012:role/ExampleRole" \
    --role-session-name "ExampleSession"
```

This command returns a set of temporary security credentials that you can use
to access AWS resources. You can export these credentials as environment
variables:

```bash
export AWS_ACCESS_KEY_ID="ASIAIOSFODNN7EXAMPLE"
export AWS_SECRET_ACCESS_KEY="wJalrXUtnFEMI/K7MDENG/bPxRfiCYEXAMPLEKEY"
export AWS_SESSION_TOKEN="AQoDYXdzEJr...<remainder of security token>"
```

Then, you can use the AWS CLI to make requests to AWS with these credentials

## 2. Python (using Boto3)

To assume a role via Python, you can use the Boto3 library. Here's an example:

```python
import boto3

# Create an STS client
sts = boto3.client('sts')

# Assume the role
response = sts.assume_role(
    RoleArn="arn:aws:iam::123456789012:role/ExampleRole",
    RoleSessionName="ExampleSession"
)

# Use the temporary credentials to create an S3 resource
s3 = boto3.resource(
    's3',
    aws_access_key_id=response['Credentials']['AccessKeyId'],
    aws_secret_access_key=response['Credentials']['SecretAccessKey'],
    aws_session_token=response['Credentials']['SessionToken']
)

# Now you can use the 's3' resource to access S3 with the permissions granted by the role
```

In this example, we're first creating an STS client, and then assuming the role using the `assume_role` method. We're then using the temporary security credentials returned by `assume_role` to create an S3 resource, which we can use to make requests to S3 with the permissions granted by the role.

Note that the initial credentials are usually pulled from the environment, just like for the CLI. Also, it's totally possible to simply feed the environment with the new values for later calls.

---

# AWS Basic building blocks

Here are some commonly used resources in EC2, VPC, ECS, and RDS, and the relationships between them:

1. **Amazon EC2 (Elastic Compute Cloud)**:
    - **Instance**: This is a virtual server in Amazon's Elastic Compute Cloud
      (EC2) for running applications on the AWS infrastructure.
    - **AMI (Amazon Machine Image)**: It's a template that contains a software
      configuration; for example, an operating system, an application server,
      and applications. Similar in concept to a Docker image, etc. From an AMI,
      you launch instances.
    - **EBS (Elastic Block Store)**: These are block-level storage volumes for
      use with Amazon EC2 instances. You can think of it as the 'hard drive'
      for your EC2 instances.

2. **Amazon VPC (Virtual Private Cloud)**:
    - **VPC**: This is a virtual network dedicated to your AWS account. It is
      logically isolated from other virtual networks in the AWS Cloud. Most of
      the time they're defined with a 10/8 IP range, split in various 10/16 or
      10/24 subnets. But there's no real limit to what you can do wrong.
    - **Subnet**: A range of IP addresses in your VPC. You can launch AWS
      resources into a subnet that you select.
    - **Internet Gateway**: This allows communication between instances in your
      VPC and the internet (usually NAT).
    - **Route Table**: Sets of rules, called routes, are used to
      determine where network traffic is directed, at the VPC and subnet levels
      (there's a default created for each VPC, that can be overriden).
    - **Security Group**: Acts as a virtual firewall for your instance,
      database, or most network-connected resource types, to filter inbound and
      outbound traffic.

3. **Amazon ECS (Elastic Container Service)**:
    - **Task Definition**: text file, in JSON format, that describes one or
      more Docker containers making up your application, together with many
      parameters. It is like a blueprint for your application.
    - **Service**: This is responsible for running and maintaining a specified
      number of instances of a task definition simultaneously in an ECS
      cluster.
    - **Cluster**: This is a logical grouping of tasks or services. If you run
      tasks or services that use the EC2 launch type, a cluster is also a
      grouping of container instances. On ECS Fargate, it's just a virtual group.

4. **Amazon RDS (Relational Database Service)**:
    - **DB Instance**: This is an isolated database environment in the AWS
      Cloud. It is the basic building block of Amazon RDS. It's basically a
      container running Postgres, MySQL or something else. For Aurora it's
      slightly different :-) 
    - **DB Subnet Group**: This is a collection of subnets designated for your
      DB instances in a VPC.
    - **DB Security Group**: This acts as a virtual firewall controlling the
      access to the DB instance.
    - **DB Parameter Group**: Allows DB instance customization (DB engine
      configuration variables, extensions, etc) 

**Relations between them**:

- The EC2 instance is the basic compute building block. You can attach an EBS volume to an EC2 instance for persistent storage.
- EC2 instances can be part of an ECS cluster and run tasks and services defined in ECS.
- EC2 instances, ECS services, and RDS DB instances all reside within a VPC, and specifically within individual subnets within the VPC.
- Security groups can be used to control inbound and outbound traffic for EC2 instances and RDS DB instances.
- Route tables and internet gateways in a VPC define how traffic is routed for the EC2 instances, ECS tasks, and RDS DB instances in the VPC.

---

# Examples for AWS policies

AWS policies define the permissions for your AWS resources. They can be simple, with just a few statements, or complex with multiple statements, conditions, and so on. Here are a couple of examples of complex AWS policies:

**Example 1: Limited S3 Bucket Access**

This policy allows a user to perform S3 actions (like `s3:PutObject`, `s3:GetObject`, `s3:DeleteObject`) only if the request is for a bucket named 'example\_bucket' and the object has a `.jpg` extension. It also allows the user to list the bucket.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowUserToSeeBucketListInTheConsole",
            "Action": ["s3:ListAllMyBuckets", "s3:HeadBucket"],
            "Effect": "Allow",
            "Resource": "*"
        },
        {
            "Sid": "AllowUserToListBucket",
            "Action": ["s3:ListBucket"],
            "Effect": "Allow",
            "Resource": ["arn:aws:s3:::example_bucket"]
        },
        {
            "Sid": "AllowUserToAllBucketActionsOnJpgFiles",
            "Action": ["s3:PutObject", "s3:GetObject", "s3:DeleteObject"],
            "Effect": "Allow",
            "Resource": ["arn:aws:s3:::example_bucket/*.jpg"]
        }
    ]
}
```

**Example 2: EC2 Actions Conditioned on Tags**

This policy allows a user to start or stop EC2 instances, but only if the instance has a tag `Owner` that matches the user's `aws:username`.

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "AllowStartStopInstancesOnlyWithOwnerTag",
            "Action": ["ec2:StartInstances", "ec2:StopInstances"],
            "Effect": "Allow",
            "Resource": "arn:aws:ec2:*:*:instance/*",
            "Condition": {
                "StringEquals": {"ec2:ResourceTag/Owner": "${aws:username}"}
            }
        },
        {
            "Effect": "Allow",
            "Action": "ec2:DescribeInstances",
            "Resource": "*"
        }
    ]
}
```

In these examples, we're using conditions (`Condition`) and variables (`${aws:username}`) to limit the permissions based on certain criteria. This makes the policies more flexible and secure.

---

# Workspace/AWS SAML workflow

the flow would look like in a SAML-based federation scenario between Google Workspace and AWS:

1. A user navigates to the Google Workspace SAML application portal and chooses
   the AWS SAML application.

2. Google Workspace authenticates the user if not already authenticated.

3. Google Workspace constructs a SAML assertion including Attribute Statements
   that contain AWS roles and the user's Google Workspace email address. Google
   Workspace signs the SAML assertion using its private key.

4. Google Workspace sends the SAML assertion back to the user's browser and
   redirects the user to the AWS SAML endpoint
   (`https://signin.aws.amazon.com/saml`).

5. The user's browser posts the SAML assertion to the AWS SAML endpoint.

6. AWS verifies the signature on the SAML assertion using Google Workspace's
   public key, and extracts the role(s) and user's email address from the
   Attribute Statements.

7. AWS generates a set of temporary security credentials (access key ID, secret
   access key, and a token) for the role(s) in the SAML assertion.

8. AWS constructs a sign-in URL for the AWS Management Console that includes
   the temporary security credentials and redirects the user's browser to this
   URL.

9. The user's browser navigates to the AWS Management Console sign-in URL, and
   the user is logged in.

The key point is that the user's Google Workspace credentials are used to
authenticate them, and AWS trusts the Google Workspace's SAML assertion because
it's signed with Google Workspace's private key. The SAML assertion contains
the information AWS needs to generate the appropriate temporary security
credentials for the user.

## AWS SAML assertion

A SAML assertion is an XML document that contains information about a user's
identity and the roles they can assume. It's digitally signed by the identity
provider (in this case, Google Workspace) to ensure its authenticity.

Here's an example of what a SAML assertion might look like:

```xml
<saml:Assertion xmlns:saml="urn:oasis:names:tc:SAML:2.0:assertion">
    <saml:AttributeStatement>
        <saml:Attribute Name="https://aws.amazon.com/SAML/Attributes/Role">
            <saml:AttributeValue>arn:aws:iam::123456789012:role/AnotherRole,arn:aws:iam::123456789012:saml-provider/GoogleWorkspace</saml:AttributeValue>
        </saml:Attribute>
        <saml:Attribute Name="https://aws.amazon.com/SAML/Attributes/RoleSessionName">
            <saml:AttributeValue>
            </saml:AttributeValue>
        </saml:Attribute>
        <saml:Attribute Name="https://aws.amazon.com/SAML/Attributes/SessionDuration">
            <saml:AttributeValue>3600</saml:AttributeValue>
        </saml:Attribute>
    </saml:AttributeStatement>
</saml:Assertion>
```

In this example, the SAML assertion contains an `AttributeStatement` with three attributes: `Role`, `RoleSessionName`, and `SessionDuration`. The `Role` attribute contains the ARN of the role the user can assume, and the ARN of the SAML provider. The `RoleSessionName` attribute contains the name of the session, and the `SessionDuration` attribute contains the duration of the session.

---

# Docker overview

Let's dive deeper into Docker's system-level workings, focusing on the build
process, images, containers, and networking:

## Docker Architecture

Docker uses a client-server architecture. The Docker client talks to the Docker
daemon, which does the heavy lifting of building, running, and managing Docker
containers. The Docker client and daemon can run on the same host, or you can
connect a Docker client to a remote Docker daemon.

## Docker Build & Images

The build process in Docker starts with a Dockerfile, which is a text document
that contains all the commands a user could call on the command line to
assemble an image. Docker reads this Dockerfile when you request a build of an
image, executes the instructions, and returns a final image.

An image is a lightweight, stand-alone, executable package that includes
everything needed to run a piece of software, including the code, a runtime,
libraries, environment variables, and config files. Images are created from
Dockerfiles, which specify the layers of images to use and the actions to take
while creating the image.

Images are stored in a Docker registry such as Docker Hub or Docker Cloud so
they can be shared with others.

## Docker Containers

A container is a runtime instance of an image—what the image becomes in memory
when executed. It's often described as a stripped-down version of a Linux
operating system.

This is made possible by a Linux kernel feature called namespaces. Docker uses
a technology called namespaces to provide the isolated workspace called the
container. When you run a container, Docker creates a set of namespaces for
that container, providing a layer of isolation: each aspect of a container runs
in a separate namespace and its access is limited to that namespace.

Docker also uses another feature of the Linux kernel, cgroups, to share
available hardware resources to containers and, if required, set up limits and
constraints.

## Networking

Docker uses network namespaces and advanced routing to provide each container
its own network stack. By default, containers can make connections to the
outside world, but the outside world cannot connect to containers. Each
outgoing connection will appear to originate from one of the IP addresses
assigned to the host.

Docker’s networking subsystem is pluggable, using drivers. When you (or Docker)
create a network, you can specify which network driver to use. Docker comes
with network drivers geared towards different use cases, like the bridge driver
for virtual networks on a single host, or the overlay driver for multi-host
    networking.

---

# Dockerfile structure

A Dockerfile is a text document that contains all the commands a user could
call on the command line to assemble an image. Using `docker build[x]`, users can
create an automated build that executes several command-line instructions in
succession.

Every instruction creates a new layer in the image, and these layers are cached
to speed up the build process. If a layer has not changed, it will not be
rebuilt. It's best to order the instructions in a Dockerfile so that the ones
that change the least are at the top, and the ones that change the most are at
the bottom. Also, it's a good practice to combine multiple `RUN` commands into
a single `RUN` command to reduce the number of layers in the image, using `&&`
to chain commands together.

## Basics

Here's a basic structure of a Dockerfile:

```Dockerfile
# Use an official Python runtime as the base image
FROM python:3.8

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install any needed packages specified in requirements.txt
RUN pip install --no-cache-dir -r requirements.txt

# Make port 80 available to the world outside this container
EXPOSE 80

# Define environment variable
ENV NAME World

# Run app.py when the container launches
CMD ["python", "app.py"]
```

## Instructions

- **Instructions**: Each instruction creates a new layer in the image. This
  allows for a high degree of flexibility and reuse in the Dockerfile.

- **Comments**: You can add comments to your Dockerfile using the `#`
  character.

- **Environment variables**: You can set environment variables using the `ENV`
  instruction.

- **FROM**: The `FROM` instruction initializes a new build stage and sets the
  base image for subsequent instructions (pulled from dockerhub by default if
  not already present in the cache)

- **MAINTAINER**: The `MAINTAINER` instruction sets the Author field of the
  generated images.

- **EXPOSE**: The `EXPOSE` instruction informs Docker that the container
  listens on the specified network ports at runtime. You can specify whether
  the port listens on TCP or UDP, and the default is TCP if the protocol is not
  specified.

- **COPY**: The `COPY` instruction copies new files or directories from `<src>`
  and adds them to the filesystem of the image at the path `<dest>`.

- **RUN**: The `RUN` instruction will execute any commands in a new layer on
  top of the current image and commit the results.

- **WORKDIR**: The `WORKDIR` instruction sets the working directory for any
  `RUN`, `CMD`, `ENTRYPOINT`, `COPY`, and `ADD` instructions that follow it in
  the Dockerfile. It is common to put the application code in a directory like
  `/app` and to declare it as the WORKDIR

- **VOLUME**: The `VOLUME` instruction creates a mount point with the specified
  name and marks it as holding externally mounted volumes from native host or
  other containers.

- **ARG**: The `ARG` instruction defines a variable that users can pass at
  build-time to the builder with the docker build command using the
  `--build-arg <varname>=<value>` flag.

- **LABEL**: The `LABEL` instruction adds metadata to an image. A LABEL is a
  key-value pair.

- **USER**: The `USER` instruction sets the user name or UID to use when
  running the image and for any `RUN`, `CMD`, and `ENTRYPOINT` instructions
  that follow it in the Dockerfile. You will often find `useradd`, `groupadd`
  and friends in a `RUN` instruction, followed by a `USER` instruction with the
  new user. It's safer than running docker under UID 0.

- **CMD**: The `CMD` instruction provides defaults for an executing container.
  These defaults can include an executable, or they can omit the executable, in
  which case the `ENTRYPOINT` instruction should be specified as well.

- **ENTRYPOINT**: The `ENTRYPOINT` instruction allows you to configure a
  container that will run as an executable.

- **HEALTHCHECK**: The `HEALTHCHECK` instruction tells Docker how to test a
  container to check that it is still working. This can detect cases such as a
  web server that is stuck in an infinite loop and unable to handle new
  connections.

- **SHELL**: The `SHELL` instruction allows the default shell used for the
  shell form of commands to be overridden. The default shell on Linux is
  `/bin/sh`.

---

# Working with Docker containers

- `docker pull <image>`: Pulls an image from a registry.
- `docker push <image>`: Pushes an image to a registry.

- `docker run <image>`: Runs a command in a new container.
  - `-d`: Runs the container in detached mode (in the background).
  - `-p`: Publishes a container's port to the host.
  - `-v`: Mounts a volume.
  - `-e`: Sets an environment variable.
  - `-it`: Runs the container in interactive mode (generally combined, `-i` stands for interactive, `-t` stands for terminal).
  - `--rm`: Automatically removes the container when it exits (a good idea)
  - `--name`: Assigns a name to the container (also a good idea)

- `docker ps`: Lists running containers.
  - `-a`: Shows all containers (default shows just running).

- `docker stop <container>`: Stops one or more running containers.
- `docker start <container>`: Starts one or more stopped containers.
- `docker rm <container>`: Removes one or more containers.

- `docker images`: Lists images.
- `docker rmi <image>`: Removes one or more images.

- `docker build <path>`: Builds an image from a Dockerfile.
  - `-t`: Names the image with a tag, for instance: `-t myimage:1.0.1`

- `docker logs <container>`: Fetches the logs of a container.
  - `-f`: Follows log output (like tail -f).

- `docker exec <container> <command>`: Runs a command in a running container,
  for instance, open a shell to check what the hell is happening in the
    container, run: `docker exec -ti <container name> /bin/bash`

- `docker inspect <object>`: Returns low-level information on Docker objects (can be used with containers, images, volumes, networks, etc).

- `docker network create <network>`: Creates a network.
- `docker network rm <network>`: Removes a network.
- `docker network connect <network> <container>`: Connects a container to a network.
- `docker network disconnect <network> <container>`: Disconnects a container from a network.

These are some of the most commonly used Docker commands and their options. Remember, you can always use `docker <command> --help` to get more information about the command and its options.

---

[1]: https://opensource.com/article/22/6/linux-cheat-command
[2]: https://tldr.sh/
